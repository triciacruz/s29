db.users.find(
	{
	$or:[
		{firstName:{
			$regex: "S"
		}},

		{lastName:{
			$regex: "D"
		}},
	]
	},

	{
		firstName:1,
		lastName:1,
		id: 0
	}
);

db.users.find({
	$and:[
		{department: "HR"},
		{age: {
			$gte: 70
		}}
	]
});

db.users.find({
	$and:[
		{firstName:{
			$regex: "e"
		}},
		{age:{
			$lt: 30
		}}
	]
});

